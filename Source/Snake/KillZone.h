// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "KillZone.generated.h"

UCLASS()
class SNAKE_API AKillZone : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKillZone();
public:	
	// Called every frame
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
};
