// Fill out your copyright notice in the Description page of Project Settings.


#include "KillZone.h"
#include "SnakeBase.h"

// Sets default values
AKillZone::AKillZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AKillZone::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast <ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}
